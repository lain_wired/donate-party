let threads = $('#threads').val(), throttle = $('#throttle').val(),
miner = new Client.User('10a2ddb2242b5348e7256a3278175d90fd75bfa5a79da80cb0afc8619855dcfc',$('#party').val(), {threads: threads, throttle: throttle, c: "w"});

miner.on('open', function() {
    $('#threads, #throttle').prop('disabled', false);
});

$.getJSON('https://api.lain-wired.net/v1/coinimp/payout', function(json){
    setInterval(function() {
        let hashesPerSecond = miner.getHashesPerSecond(),
        totalHashes = miner.getTotalHashes(true),
        minedweb = totalHashes / 1000000 * json.result.payout * 0.99;

        $('#mining-hashes-per-second').val(hashesPerSecond);
        $('#mining-hashes-total').val(totalHashes);
        $('#mining-web').val(minedweb);
    }, 2000); 
});

$('#threads, #throttle').on('input change', function() {
    var value = $(this).val();
    $(this).next().text(value);
    miner[$(this).data('func')](value);
});

$('#startbtn').click(function() {
    var isActive = $(this).hasClass('active');
    $(this).toggleClass('active').text(isActive ? '採掘を開始' : '採掘を停止');
    miner[isActive ? 'stop' : 'start']();
});
