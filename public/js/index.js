$.getJSON('https://api.lain-wired.net/v1/coinmarketcap/price', function(price){

    $('#btc-jpy').html(price.result.btc);
    $('#web-jpy').html(price.result.web);

    $.getJSON('https://api.lain-wired.net/v1/coinimp/user/donate-party.org', function(users){

        for (let user in users.result) {

            $('#' + user + '-web').html(users.result[user]);
            $('#' + user + '-jpy').html(Math.ceil(users.result[user] * price.result.web * 100) / 100);
        }
    });

    $.getJSON('https://blockchain.info/rawaddr/bc1qcmty0z85t4xh02l3cam0emk9qrsg5z8hfzzn8n', function(ad){

        $('#ad-btc').html(ad.final_balance * 0.00000001);
        $('#ad-jpy').html(Math.ceil(ad.final_balance * 0.00000001 * price.result.btc * 100) / 100);
    });
});
