# CoinIMPで政党を支援しよう
アドレス
---
https://donate-party.org/

Special Thanks
---
* [きく_いけ_そう](https://note.com/so_harunohi)さん(Twitter[@so_harunohi](https://twitter.com/so_harunohi))
    * アイコン作成

注意事項
---
* `js/miner.js`
    * 公開鍵を自分のものに変更すること

## License
GNU GPLv2
